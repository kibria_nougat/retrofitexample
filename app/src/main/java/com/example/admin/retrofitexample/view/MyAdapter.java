package com.example.admin.retrofitexample.view;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.admin.retrofitexample.R;
import com.example.admin.retrofitexample.model.MyData;

import java.util.ArrayList;

/**
 * Created by Admin on 18-Feb-18.
 */

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

    ArrayList<MyData> dataArrayList;
    Context context;

    public MyAdapter(Context context, ArrayList<MyData> dataArrayList) {
        this.dataArrayList = dataArrayList;
        this.context = context;
    }


    public void setList(ArrayList<MyData> dataArrayList){
        if(this.dataArrayList.size() != 0){
            this.dataArrayList.clear();
            this.dataArrayList.addAll(dataArrayList);
            notifyDataSetChanged();
        }
        else {
            this.dataArrayList.addAll(dataArrayList);
            notifyDataSetChanged();
        }
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView textView;

        public MyViewHolder(View view) {
            super(view);
            textView = view.findViewById(R.id.text);
        }
    }

    @Override
    public MyAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.list_row_layout, parent, false);
        MyViewHolder viewHolder = new MyViewHolder(view);
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(MyAdapter.MyViewHolder holder, final int position) {
        holder.textView.setText(dataArrayList.get(position).getId() + "\n" + dataArrayList.get(position).getName());
        holder.textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //        Toast.makeText(context, "clicked", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(context, DetailsActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("DETAILS_TEXT", dataArrayList.get(position));
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataArrayList.size();
    }

}
