package com.example.admin.retrofitexample.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Admin on 17-Apr-18.
 */

public class ProgramData {
    @SerializedName("Sheet1")
    @Expose
    private List<Sheet1> sheet1 = null;

    public List<Sheet1> getSheet1() {
        return sheet1;
    }

    public void setSheet1(List<Sheet1> sheet1) {
        this.sheet1 = sheet1;
    }
}
