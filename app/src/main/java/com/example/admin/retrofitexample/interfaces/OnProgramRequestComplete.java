package com.example.admin.retrofitexample.interfaces;

import com.example.admin.retrofitexample.model.Sheet1;

import java.util.ArrayList;

/**
 * Created by Admin on 17-Apr-18.
 */

public interface OnProgramRequestComplete {
    void onRequestSuccess(ArrayList<Sheet1> programDataArrayList);
    void onRequestError(String errorMessage);
}
