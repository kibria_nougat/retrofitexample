package com.example.admin.retrofitexample.view;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;

import com.example.admin.retrofitexample.R;
import com.example.admin.retrofitexample.interfaces.ActivityViewInterface;
import com.example.admin.retrofitexample.model.MyData;
import com.example.admin.retrofitexample.presenter.MyActivityPresenter;
import com.example.admin.retrofitexample.utils.NetworkUtil;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements ActivityViewInterface {


    RecyclerView recyclerView;
    MyAdapter myAdapter;
    ArrayList<MyData> arrayList = new ArrayList<>();
    ProgressDialog pd;
    MyActivityPresenter myActivityPresenter;
    ConstraintLayout main_layout;
    BroadcastReceiver internetConnectivityChangeReceiver;
    int last_Visible = 10;
    Snackbar snackbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.recyclerView);
        main_layout = findViewById(R.id.main_layout);
        pd = new ProgressDialog(this);
        pd.setMessage("loading");

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        myAdapter = new MyAdapter(getApplicationContext(), arrayList);
        recyclerView.setAdapter(myAdapter);

        myActivityPresenter = new MyActivityPresenter(this);
       // myActivityPresenter.getData(0, 1500);
        myActivityPresenter.getProgramData();

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                LinearLayoutManager layoutManager = LinearLayoutManager.class.cast(recyclerView.getLayoutManager());
                int lastVisible = layoutManager.findLastVisibleItemPosition() + 1;
                last_Visible = lastVisible;

                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_FLING) {
                    // Do something

                } else if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    // Do something

                } else {
                    // Do something
                    if (lastVisible % 10 == 0) {
                        //myActivityPresenter.getData(0, lastVisible + 10);
                        Log.d("DoSomething", "DoSomething 3 " + lastVisible);
                    }

                }

            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });

        netConnectionAvailability();

    }

    private void netConnectionAvailability() {

        internetConnectivityChangeReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                int status = NetworkUtil.getConnectivityStatusInt(context);
                if (status == 0) {
                    showSnackBar();
                    Log.d("NetConnectivityStatus", "Not Available");
                } else {

                    if(isInitialStickyBroadcast()){
                        // Do Nothing;
                    }
                    else {
                        hideSnackBar();
                        Log.d("NetConnectivityStatus", "Available");
                        //myActivityPresenter.getData(0, last_Visible + 10);
                        myActivityPresenter.getProgramData();
                    }

                }



            }
        };

    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver();
        Log.d("OnResume", "OnResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver();
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("onStop", "onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("onDestroy", "onDestroy");
    }

    @Override
    public void showProgressDialog() {
        if (pd != null)
            pd.show();
    }

    @Override
    public void hideProgressDialog() {
        if (pd != null)
            pd.hide();
    }

    @Override
    public void showDataInRecyclerView(ArrayList<MyData> myDataArrayList) {
        Log.d("DoSomething", "myDataArrayList size " + myDataArrayList.size());
        myAdapter.setList(myDataArrayList);
    }

    @Override
    public void showDataInTextView(ArrayList<MyData> myDataArrayList) {

    }

    public void showSnackBar() {
        snackbar = Snackbar.make(main_layout, "Network Not Available!", Snackbar.LENGTH_LONG)
                .setAction("Connect", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        startActivity(new Intent(android.provider.Settings.ACTION_SETTINGS));
                    }
                });

        snackbar.setDuration(10000)
                .show();
    }

    public void hideSnackBar() {
        if (snackbar != null && snackbar.isShown()) {
            snackbar.dismiss();
        }
    }

    private void registerReceiver() {
        registerReceiver(internetConnectivityChangeReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
    }

    private void unregisterReceiver() {
        unregisterReceiver(internetConnectivityChangeReceiver);
    }

}
