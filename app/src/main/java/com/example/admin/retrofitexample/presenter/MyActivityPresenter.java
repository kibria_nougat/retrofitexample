package com.example.admin.retrofitexample.presenter;

import android.util.Log;

import com.example.admin.retrofitexample.interfaces.ActivityViewInterface;
import com.example.admin.retrofitexample.interfaces.OnProgramRequestComplete;
import com.example.admin.retrofitexample.interfaces.OnRequestComplete;
import com.example.admin.retrofitexample.model.InvokeAPI;
import com.example.admin.retrofitexample.model.MyData;
import com.example.admin.retrofitexample.model.ProgramData;
import com.example.admin.retrofitexample.model.Sheet1;

import java.util.ArrayList;

/**
 * Created by Admin on 19-Feb-18.
 */

public class MyActivityPresenter {

    ActivityViewInterface activityViewInterface;

    public MyActivityPresenter(ActivityViewInterface activityViewInterface){
        this.activityViewInterface = activityViewInterface;
    }

    public void getData(int from, int limit){
        Log.d("TimeTesting", "Request Started");
        activityViewInterface.showProgressDialog();
        InvokeAPI invokeAPI = new InvokeAPI();
        invokeAPI.makeApiRequest(from, limit, new OnRequestComplete() {
            @Override
            public void onRequestSuccess(ArrayList<MyData> myDataArrayList) {
                activityViewInterface.showDataInRecyclerView(myDataArrayList);
                activityViewInterface.hideProgressDialog();
            }

            @Override
            public void onRequestError(String errorMessage) {

            }
        });
    }

    public void getDetailsData(String id){
        Log.d("getDetailsData" , "Called "+id);
        activityViewInterface.showProgressDialog();
        InvokeAPI invokeAPI = new InvokeAPI();
        invokeAPI.makeApiRequest(id, new OnRequestComplete() {
            @Override
            public void onRequestSuccess(ArrayList<MyData> myDataArrayList) {
                activityViewInterface.showDataInTextView(myDataArrayList);
                activityViewInterface.hideProgressDialog();
            }

            @Override
            public void onRequestError(String errorMessage) {

            }
        });
    }


    public void getProgramData(){
        Log.d("TimeTesting" , "Called");
        activityViewInterface.showProgressDialog();
        InvokeAPI invokeAPI = new InvokeAPI();
        invokeAPI.makeApiRequest(new OnProgramRequestComplete() {
            @Override
            public void onRequestSuccess(ArrayList<Sheet1> programDataArrayList) {
               Log.d("ProgramTesting", programDataArrayList.size()+" ");
                activityViewInterface.hideProgressDialog();
            }

            @Override
            public void onRequestError(String errorMessage) {
                Log.d("ProgramTesting"," onRequestError"+errorMessage);
            }
        });

    }



}
