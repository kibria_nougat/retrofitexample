package com.example.admin.retrofitexample.model;

import android.util.Log;

import com.example.admin.retrofitexample.interfaces.OnProgramRequestComplete;
import com.example.admin.retrofitexample.interfaces.OnRequestComplete;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Admin on 19-Feb-18.
 */

public class InvokeAPI {

    public void makeApiRequest(int from , int limit, final OnRequestComplete onRequestComplete) {

        final String BASE_URL = "https://api.coinmarketcap.com/";
        ApiInterface apiInterface = RetrofitClient.getClient(BASE_URL).create(ApiInterface.class);
        apiInterface.getAnswers(from,limit).enqueue(new Callback<List<MyData>>() {
            @Override
            public void onResponse(Call<List<MyData>> call, Response<List<MyData>> response) {
                ArrayList<MyData> myData = new ArrayList<>();
                myData.addAll(response.body());
                Log.d("TimeTesting", "First Req DataFound "+myData.size());
                onRequestComplete.onRequestSuccess(myData);
            }

            @Override
            public void onFailure(Call<List<MyData>> call, Throwable t) {
                Log.d("TimeTesting", "First Req  DataNotFound");
                onRequestComplete.onRequestError(t.toString());
            }

        });
    }

    public void makeApiRequest(String id, final OnRequestComplete onRequestComplete) {

        final String BASE_URL = "https://api.coinmarketcap.com/";
        ApiInterface apiInterface = RetrofitClient.getClient(BASE_URL).create(ApiInterface.class);
        apiInterface.getDetails("v1/ticker/"+id).enqueue(new Callback<List<MyData>>() {
            @Override
            public void onResponse(Call<List<MyData>> call, Response<List<MyData>> response) {

                ArrayList<MyData> myData = new ArrayList<>();
                myData.addAll(response.body());
                Log.d("OnResponse", "Second Req DetailsDataFound "+myData.size());
                onRequestComplete.onRequestSuccess(myData);
            }

            @Override
            public void onFailure(Call<List<MyData>> call, Throwable t) {
                Log.d("OnResponse", "Second Req DetailsDataNotFound");
                onRequestComplete.onRequestError(t.toString());
            }
        });

    }

    public void makeApiRequest(final OnProgramRequestComplete onProgramRequestComplete) {

        final String BASE_URL = "https://script.google.com/macros/s/AKfycbygukdW3tt8sCPcFDlkMnMuNu9bH5fpt7bKV50p2bM/";
        ApiInterface apiInterface = RetrofitClient.getClient(BASE_URL).create(ApiInterface.class);
        apiInterface.getProgramList("1EXWQfsLuPwNCfHNh01b8a7ylxISYp6VW4PYXEWdph5I", "Sheet1").enqueue(new Callback<ProgramData>() {
            @Override
            public void onResponse(Call<ProgramData> call, Response<ProgramData> response) {

                ArrayList<Sheet1> myData = new ArrayList<>();
                myData.addAll(response.body().getSheet1());
                Log.d("TimeTesting", "Second Req DetailsDataFound "+myData.size());
                onProgramRequestComplete.onRequestSuccess(myData);
            }

            @Override
            public void onFailure(Call<ProgramData> call, Throwable t) {
                Log.d("TimeTesting", "Second Req DetailsDataNotFound");
                onProgramRequestComplete.onRequestError(t.toString());
            }
        });

    }

}
