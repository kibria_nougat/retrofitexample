package com.example.admin.retrofitexample.interfaces;

import com.example.admin.retrofitexample.model.MyData;

import java.util.ArrayList;

/**
 * Created by Admin on 19-Feb-18.
 */

public interface ActivityViewInterface {
    void showProgressDialog();
    void hideProgressDialog();
    void showDataInRecyclerView(ArrayList<MyData> myDataArrayList);
    void showDataInTextView(ArrayList<MyData> myDataArrayList);
}
