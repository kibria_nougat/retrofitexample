package com.example.admin.retrofitexample.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Admin on 17-Apr-18.
 */

public class Sheet1 {
    @SerializedName("program_id")
    @Expose
    private String programId;
    @SerializedName("program_name")
    @Expose
    private String programName;
    @SerializedName("program_host_name")
    @Expose
    private String programHostName;
    @SerializedName("program_start_time")
    @Expose
    private String programStartTime;
    @SerializedName("program_end_time")
    @Expose
    private String programEndTime;
    @SerializedName("program_duration")
    @Expose
    private String programDuration;

    public String getProgramId() {
        return programId;
    }

    public void setProgramId(String programId) {
        this.programId = programId;
    }

    public String getProgramName() {
        return programName;
    }

    public void setProgramName(String programName) {
        this.programName = programName;
    }

    public String getProgramHostName() {
        return programHostName;
    }

    public void setProgramHostName(String programHostName) {
        this.programHostName = programHostName;
    }

    public String getProgramStartTime() {
        return programStartTime;
    }

    public void setProgramStartTime(String programStartTime) {
        this.programStartTime = programStartTime;
    }

    public String getProgramEndTime() {
        return programEndTime;
    }

    public void setProgramEndTime(String programEndTime) {
        this.programEndTime = programEndTime;
    }

    public String getProgramDuration() {
        return programDuration;
    }

    public void setProgramDuration(String programDuration) {
        this.programDuration = programDuration;
    }

}