package com.example.admin.retrofitexample.view;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.example.admin.retrofitexample.R;
import com.example.admin.retrofitexample.interfaces.ActivityViewInterface;
import com.example.admin.retrofitexample.model.MyData;
import com.example.admin.retrofitexample.presenter.MyActivityPresenter;

import java.util.ArrayList;

public class DetailsActivity extends AppCompatActivity implements ActivityViewInterface {

    TextView detailsText;
    MyActivityPresenter myActivityPresenter;
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        detailsText = findViewById(R.id.detailsText);
        pd = new ProgressDialog(this);
        pd.setMessage("loading");

        MyData myData = (MyData) getIntent().getExtras().getSerializable("DETAILS_TEXT");

        myActivityPresenter = new MyActivityPresenter(this);
        myActivityPresenter.getDetailsData(myData.getId().toString());
    }


    @Override
    public void showDataInRecyclerView(ArrayList<MyData> myDataArrayList) {

    }

    @Override
    public void showDataInTextView(ArrayList<MyData> myDataArrayList) {
        Log.d("DataFoundTest", "" + myDataArrayList.get(0).getId() + " " + myDataArrayList.get(0).getName());
        MyData myData = myDataArrayList.get(0);
        detailsText.setText(myData.getId() + "\n" + myData.getName() + "\n"+myData.get24hVolumeUsd()+"\n"+ myData.getAvailableSupply());
    }

    @Override
    public void showProgressDialog() {
        if (pd != null)
            pd.show();
    }

    @Override
    public void hideProgressDialog() {
        if (pd != null)
            pd.hide();
    }
}
