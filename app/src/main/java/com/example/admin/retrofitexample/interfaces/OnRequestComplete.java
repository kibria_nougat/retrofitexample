package com.example.admin.retrofitexample.interfaces;

import com.example.admin.retrofitexample.model.MyData;

import java.util.ArrayList;

/**
 * Created by Admin on 19-Feb-18.
 */

public interface OnRequestComplete {
    void onRequestSuccess(ArrayList<MyData> myDataArrayList);
    void onRequestError(String errorMessage);
}
