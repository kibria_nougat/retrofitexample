package com.example.admin.retrofitexample.model;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.Url;

/**
 * Created by Admin on 18-Feb-18.
 */

public interface ApiInterface {

    @GET("v1/ticker/")
    Call<List<MyData>> getAnswers(@Query("start") int from, @Query("limit") int limit);

    @GET
    Call<List<MyData>> getDetails(@Url String url);

    @GET("exec?")
    Call<ProgramData> getProgramList(@Query("id") String id, @Query("sheet") String sheet);
}
